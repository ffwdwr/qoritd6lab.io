---
title: Star Wars D6
---

# Star Wars D6

This is an introduction to the usage of Star Wars D6 as a roll system for the
guild. It specifically aims to cover character creation, usage, and custom
rules. It also covers clarification and repetition of rules that are likely to
be important.

You can find the PDF for Star Wars D6 [here](/swd6.pdf). You *will* need this
PDF to look up certain rules, although this document attempts to cover the
basics as concisely as possible.

There is also a [Galaxy Guide for The Old Republic](/tor.pdf), which you may
care to read for history, or extra species options and equipment. If there is a
difference between the core book and this supplement, the core book takes
precedence.

The main draw of Star Wars D6 is that it is a relatively simple system that is
tailor-fit for "a galaxy far, far away". Unfortunately, there are a few things
inside the system that make it less fit for online use, usually because it
sacrifices expediency for detail. This document proposes a few changes to keep
things moving.

The use of the system comes with a plea to Keep Things Simple. While it is
certainly possible to do advanced things with the system, and it is sometimes
good to do advanced things, it might often be more desireable to just do the
simple thing.

## Character creation

Character sheets in Star Wars D6 are remarkably simple, but a little bit of
complexity goes into creating them. Examples go a long way to making this
process as simple as possible. Find [here](/sheet.svg) a filled-in example
sheet.

Going through the sheet:

- Name, gender, age, height, weight, and physical description all speak for
  themselves.

- Species determines a couple of variables that will be obvious later.

- Type is your own description of the "class" of your character. It can be
  anything.

- Dexterity, Knowledge, Mechanical, Perception, Strength, and Technical are your
  **attributes**. The "3D+1" score next to Technology means that you may roll
  three six-sided dice and add one to the result to determine what you have
  rolled.

- The items listed below the attributes are your **skills**. They are
  narrowed-down versions of the attributes. If no value is listed beside the
  skill, you roll the same as your attribute dice. If a value is listed beside
  the skill, you roll that instead.

- The Move value determines how many metres you can walk normally in a single
  round. More on this later.

- The rest of the sheet is unimportant.

### Point buy

To put dice into the attributes and skills, the guild uses a point buy system.
First, you rank your character at a certain level. Your character's level
determines how many points they get.

| Level |                Description                |  Attribute dice   | Skill dice |
| :---: | :---------------------------------------: | :---------------: | :--------: |
|   1   |      Fresh from the Imperial Academy      | Your species + 6D |     7D     |
|   2   |      A moderately experienced agent       | Your species + 6D |    14D     |
|   3   | An agent with some years under their belt | Your species + 7D |    21D     |
|   4   |              An expert agent              | Your species + 8D |    28D     |
|   5   |          A master at their trade          | Your species + 9D |    35D     |

When purchasing attributes, you must remain between the minimum and maximum
determined by your species. For instance, a twi'lek must put at least 2 dice
into Dexterity, and can put no more than 4 dice into Dexterity.

You can purchase any skills you want. When purchasing skills, you don't need to
spend points up to your attribute score. For instance, if your Dexterity is 2D,
it only costs 1 point to have 3D in Blaster. When/if you later increase
Dexterity, your Blaster skill increases as well.

Advanced skills cost double the amount of skill dice. Specialisations cost half
the amount of skill dice. See page 21 and 22 for the definitions of these terms.

One important piece of information: You can split up a single die into three
"pips" when doing point buy. Page 23 describes this in more details, but it
works like this:

| Dice points | Pips  | Die roll |
| :---------: | :---: | :------: |
|      1      |   3   |    1D    |
|     1⅓      |   4   |   1D+1   |
|     1⅔      |   5   |   1D+2   |
|      2      |   6   |    2D    |

At level 3 or higher, you can exceed the attribute maximum of your species by 2
pips.

### Skills

Page 34 has a list of all possible skills. Skills are generally fairly specific,
and as such there are a lot of them. Fortunately, most won't be applicable to
your character.

Find here a list of skills that are likely to be picked:

- Blaster (DEX) - Shoot blaster weapons.
- Brawling (STR) - Punch people.
- Brawling Parry (DEX) - Defend yourself with your hands.
- Computer Programming/Repair (TEC) - Slice computers.
- Con (PER) - Deceive people.
- Demolitions (TEC) - Blow things up.
- Dodge (DEX) - Dodge out of the way of blaster shots.
- Firearms (DEX) - Shoot slugthrower weapons.
- First Aid (TEC) - Patch up wounds.
- Grenade (DEX) - Throw grenades.
- Ground Vehicle Operation (MEC) - Ride speeders.
- Hide (PER) - Hide items on your person.
- Intimidation (KNO) - Intimidate people.
- Investigation (PER) - Investigate locations and read people.
- (A) Medicine (TEC) - Perform complex medical procedures.
- Melee Combat (DEX) - Use melee weapons.
- Melee Parry (DEX) - Defend yourself with melee weapons.
- Persuasion (PER) - Persuade people.
- Security (TEC) - Slice past security.
- Sneak (PER) - Be stealthy.

### Species

Your species determines the following things for you:

- How many attribute dice you start out with.
- What your minimum and maximum dice are in certain attributes.
- Any special abilities.
- Some story fluff.
- Your Move speed.

You can find the stat block for your species in the PDFs linked at the top of
this document.

Find below the entry for twi'leks. It should be fairly self-explanatory. All
numbers on the left side of the divide are minimums, and all numbers on the
right side are maximums.

#### Twi'lek

**Attribute Dice:** 12D\
**DEXTERITY 2D/4D+1**\
**KNOWLEDGE 1D/4D**\
**MECHANICAL 1D/3D**\
**PERCEPTION 2D/4D+2**\
**STRENGTH 1D/3D+2**\
**TECHNICAL 1D/3D**\
**Special Abilities:**\
*Tentacles*: Twi'leks can use their tentacles to communicate in secret
with each other, even if in a room full of individuals. The complex
movement of the tentacles is, in a sense, a "secret" language that
all Twi'leks are fluent in.\
*Skill Bonus*: At the time the character is generated only, the
character receives 2D for every 1D placed in the persuasion skill.\
**Story Factors:**\
*Slavery*: Slavery is so ingrained as the main trade of Ryloth, that most
Twi'leks are generally thought to be either a slave or consort of
some kind, and often treated as second class citizens, this is espe­cially true
in Hutt space.\
**Move:** 10 / 12\
**Size:** 1.6-2.0 meters tall

### Equipment

Equipment is explained on [this page](/equipment).

## Usage

The core of the D6 system is extremely simple: You roll the dice that it says
next to your skill or attribute, and the GM determines whether you succeeded or
not by comparing your roll to the difficulty that the GM had decided on.

**If a character's skill roll is equal to or higher than the difficulty, they
succeed.**

Use the below table to determine difficulty:

|   Difficulty   | Difficulty Numbers | Average |
| :------------: | :----------------: | :-----: |
|   Very Easy    |        1-5         |    3    |
|      Easy      |        6-10        |    8    |
|    Moderate    |       11-15        |   13    |
|   Difficult    |       16-20        |   18    |
| Very Difficult |       21-30        |   25    |
|     Heroic     |        31+         |    —    |

Example:

> Heria wants to disable the alarm system of a Republic military facility. The
> DM asks her to roll Security to see if it works. She has a Security of 6D6+2,
> and rolls a total of 24+2. This is well above the Moderate difficulty of 13
> determined by the GM, and so she succeeds.

### Combat

Combat is explained on [this page](/combat).

### Scrapped rules

The rules book list a lot of rules. To keep things simple, these rules are
scrapped:

- (Chapter 2) Character creation is done through a custom point buy method. This
  chapter can largely be ignored.

- (Chapter 2) Advantages and disadvantages are not used.

- (Chapter 5) No wild (exploding) dice.

- (Chapter 5) No dice simplification; all dice are always rolled.

- (Chapter 5) No Character Points and Force Points.

- (Chapter 6) Drawing a weapon and setting it to stun are both free actions.

- (Chapter 6) Ammunition is not tracked.

- (Chapter 6) Mechanics for protection (e.g., the table behind which you are
  hiding taking damage) are not used.

- (Chapter 6) Weapons and armour damage is optional.

- (Chapter 8) TODO Space combat is currently not implemented at all.

- (Chapter 9) TODO The Force is currently not implemented.

### Changed rules

- (Chapter 5) You can only take one action per round, plus reactions, plus
  movement. This is concisely described in [Combat](/combat). This keeps things
  expedient. If you really want to do multiple actions in a round and you
  believe the circumstances are exceptional, look up the rules and apply them.

- (Chapter 5) Initiative is changed to be more D&D-like, see [Combat](/combat).

- (Chapter 6) TODO Grenades

- (Chapter 6) TODO Simplify cover

- (Chapter 6/7) TODO Movement/ranges

### Rule clarifications

- TODO
